import { YearModel, JobModel, UrlTitleModel, ProyectModel } from "@/models";

export const PROYECTS: ProyectModel[] = [
  {
    title: "Safe Route Mayotte",
    code: "",
    description: "project.safe_route_mayotte",
    links: [
      { url: "https://saferoutemayotte.com/", title: "Safe Route Mayotte" }
    ],
    chips: [
      { url: "https://www.openstreetmap.org/", title: "OpenStreetMap" },
      { url: "https://vuejs.org/", title: "VueJs" },
      { url: "https://quasar-framework.org/", title: "Quasar" },
      { url: "https://firebase.google.com/", title: "Firebase" }
    ]
  },
  {
    title: "My Social Coin",
    code: "",
    description: "project.social_coin",
    links: [
      { url: "https://my-social-coin.web.app/", title: "My Social Coin" }
    ],
    chips: [
      { url: "https://en.wikipedia.org/wiki/QR_code", title: "QR Code" },
      { url: "https://vuejs.org/", title: "VueJs" },
      { url: "https://quasar-framework.org/", title: "Quasar" },
      { url: "https://firebase.google.com/", title: "Firebase" }
    ]
  },
  {
    title: "Cronos Escape City",
    code: "",
    description: "project.cronos",
    links: [{ url: "https://cronosescape.com", title: "Cronos Escape App" }],
    chips: [
      { url: "https://vuejs.org/", title: "VueJs" },
      { url: "https://quasar-framework.org/", title: "Quasar" },
      { url: "https://firebase.google.com/", title: "Firebase" }
    ]
  },
  {
    title: "Tienda Online",
    code: "",
    description: "project.tiendaonline",
    links: [{ url: "https://mielgrandujo.com", title: "MielGrandDujo" }],
    chips: [
      { url: "https://es.wordpress.com/", title: "Wordpress" },
      { url: "https://woocommerce.com/", title: "Woocommerce" },
      { url: "https://stripe.com/", title: "Stripe" },
      {
        url:
          "https://es.wikipedia.org/wiki/Posicionamiento_en_buscadores#:~:text=El%20posicionamiento%20en%20buscadores%2C%20optimizaci%C3%B3n,u%20otros%20buscadores%20de%20internet",
        title: "SEO"
      }
    ]
  },
  {
    title: "Music Live Map",
    code: "https://github.com/RaulCE22/LiveMusicMap",
    description: "project.musiclivemap",
    links: [{ url: "http://bit.do/musicmap", title: "Music Map" }],
    chips: [
      { url: "https://vuejs.org/", title: "VueJs" },
      { url: "https://quasar-framework.org/", title: "Quasar" },
      { url: "https://firebase.google.com/", title: "Firebase" }
    ]
  },
  {
    title: "Covuaturage le la",
    code: "https://github.com/RaulCE22",
    description: "project.covuaturage",
    links: [
      {
        url: "https://www.linfo.re/tags/embouteillages",
        title: "La reunion - Embouteillages"
      }
    ],
    chips: [
      { url: "https://vuejs.org/", title: "VueJs" },
      { url: "https://quasar-framework.org/", title: "Quasar" },
      { url: "https://firebase.google.com/", title: "Firebase" }
    ]
  },
  {
    title: "SelfieMaton",
    code:
      "https://github.com/RaulCE22/Adeter/tree/master/Selfiematon/Raspberrypi",
    description: "project.selfiematon",
    links: [
      {
        url: "http://www.adeter.org/594/",
        title: "SelfieMaton en Canal Extremadura"
      }
    ],
    chips: [
      { url: "https://www.python.org/", title: "Pyhton" },
      { url: "https://www.raspberrypi.org/", title: "Raspberry Pi" },
      { url: "https://www.dropbox.com/es/", title: "Dropbox" },
      { url: "https://ifttt.com/", title: "IFTTT" },
      { url: "https://es-es.facebook.com/", title: "Facebook" }
    ]
  },
  {
    title: "InfoSolidario",
    code: "https://github.com/RaulCE22/InfoSolidario/tree/master/infosolidario",
    description: "project.infosolidario",
    links: [
      {
        url: "https://saladeprensa.usal.es/node/50732",
        title: "Universidad de Salamanca - Prensa"
      }
    ],
    chips: [{ url: "https://rubyonrails.org/", title: "Ruby On Rails" }]
  }
];

export const COMPETENCES: UrlTitleModel[] = [
  { url: "https://vuejs.org/", title: "VueJS" },
  { url: "https://angular.io/", title: "Angular" },
  { url: "https://ngrx.io/", title: "NGRX" },
  { url: "https://rxjs-dev.firebaseapp.com/", title: "RXJS" },
  { url: "https://cordova.apache.org/", title: "Cordova" },
  { url: "https://ionicframework.com/", title: "Ionic" },
  { url: "https://es.wordpress.com/", title: "Wordpress" },
  { url: "https://woocommerce.com/", title: "Woocommerce" },
  { url: "https://stripe.com/", title: "Stripe" },
  { url: "https://quasar.dev/", title: "Quasar" },
  { url: "https://www.primefaces.org/primeng/", title: "PrimeNG" },
  { url: "https://www.python.org/", title: "Python" },
  { url: "https://firebase.google.com/", title: "Firebase" },
  { url: "https://cloud.google.com/", title: "GCloud" },
  { url: "https://nodejs.org/es/", title: "NodeJS" },
  { url: "https://www.mongodb.com/", title: "MongoDB" },
  { url: "https://www.mysql.com/", title: "SQL" }
];

export const JOBS: JobModel[] = [
  {
    title: "jobs.cronos.title",
    description: "jobs.cronos.description",
    image: "cronos.png",
    company: "Cronos Escape City",
    dateStart: new Date("7/01/2020"),
    dateEnd: new Date()
  },
  {
    title: "jobs.essengrp.title",
    description: "jobs.essengrp.description",
    image: "essencegrp.png",
    company: "Essence Group",
    dateStart: new Date("01/01/2018"),
    dateEnd: new Date("04/01/2020")
  },
  {
    title: "jobs.profuturo.title",
    description: "jobs.profuturo.description",
    image: "profuturo.jpg",
    company: "Fundacion Profuturo",
    dateStart: new Date("08/01/2016"),
    dateEnd: new Date("01/01/2018")
  },
  {
    title: "jobs.ted.title",
    description: "jobs.ted.description",
    image: "ted.png",
    company: "Telefonica educacion digital",
    dateStart: new Date("08/01/2015"),
    dateEnd: new Date("10/01/2016")
  }
];

export const YEARS: YearModel[] = [
  {
    color: "cyan",
    year: "2016",
    school: "academy.uni_sala",
    name: "academy.doctor",
    icon: "dii.png"
  },
  {
    color: "green",
    year: "2014",
    school: "academy.uni_sala",
    name: "academy.master",
    icon: "mayr.png"
  },
  {
    color: "pink",
    year: "2013",
    school: "academy.uni_sala",
    name: "academy.curso_web",
    icon: "cdw.png"
  },
  {
    color: "amber",
    year: "2013",
    school: "academy.uni_extre",
    name: "academy.degree",
    icon: "itis.png"
  },
  {
    color: "orange",
    year: "2012",
    school: "academy.uni_extre",
    name: "academy.title_monitor",
    icon: "moytl.png"
  }
];
