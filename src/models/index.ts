export interface UrlTitleModel {
  url: string;
  title: string;
}
export interface ProyectModel {
  title: string;
  code: string;
  description: string;
  links: UrlTitleModel[];
  chips: UrlTitleModel[];
}
export interface JobModel {
  title: string;
  description: string;
  image: string;
  company: string;
  dateStart: Date;
  dateEnd: Date;
}
export interface YearModel {
  color: string;
  year: string;
  school: string;
  name: string;
  icon: string;
}
